export default class FolderNode {
    public key!: string;
    public label!: string;
    public data?: any;
    public icon!: string;
    public children: FolderNode[] = [];
    public leaf!: boolean;

    constructor(init?: Partial<FolderNode>) {
        Object.assign(this, init);
    }
}
