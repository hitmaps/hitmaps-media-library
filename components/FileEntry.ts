export default class FileEntry {
    public fileName!: string;
    public localizedTextName?: string;
    public fullPath!: string;

    constructor(init?: Partial<FileEntry>) {
        Object.assign(this, init);
    }
}